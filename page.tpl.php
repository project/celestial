<?php
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language ?>" lang="<?php print $language->language ?>" dir="<?php print $language->dir ?>">

<head>
  <?php print $head; ?>
  <title><?php print $head_title; ?></title>
  <?php print $styles; ?>
  <?php print $scripts; ?>
  <script type="text/javascript"><?php /* Needed to avoid Flash of Unstyled Content in IE */ ?> </script>
</head>

<body class="<?php print $body_classes; ?>">
  <div id="header">
    <div class="top">
      <?php if (!empty($secondary_links)): ?>
      <div id="secondary" class="clear-block">
        <?php print theme('links', $secondary_links, array('class' => 'links secondary-links')); ?>
      </div>
      <?php endif; ?>
    </div><!--/top-->

    <?php if (!empty($site_name)): ?>
    <h1 id="site-info"><a href="<?php print $front_page ?>" title="<?php print t('Home'); ?>" rel="home"><?php print $site_name; ?> </a><?php if (!empty($site_slogan)): ?>
      <span class="site-slogan"><?php print $site_slogan; ?></span>
    <?php endif; ?>
    </h1>
    <?php endif; ?>

    <div id="navigation" class="menu <?php if (!empty($primary_links)) { print "withprimary"; } if (!empty($secondary_links)) { print " withsecondary"; } ?> ">
      <?php if (!empty($primary_links)): ?>
      <div id="primary" class="clear-block">
        <?php print theme('links', $primary_links, array('class' => 'links primary-links')); ?>
      </div>
      <?php endif; ?>

      <div class="tree"></div>

    </div><!--/header-->
  </div>
  <div id="container">
    <div id="content">
      <?php if (!empty($breadcrumb)): ?><div id="breadcrumb"><?php print $breadcrumb; ?></div><?php endif; ?>
      <?php if (!empty($mission)): ?><div id="mission" class="curved"><span><?php print $mission; ?></span></div><?php endif; ?>
      <?php if (!empty($title)): ?><h1 class="title" id="page-title"><?php print $title; ?></h1><?php endif; ?>
      <?php if (!empty($tabs)): ?><div class="tabs"><?php print $tabs; ?></div><?php endif; ?>
      <?php if (!empty($messages)): print $messages; endif; ?>
      <?php if (!empty($help)): print $help; endif; ?>
        <div id="content-content" class="clear-block">
	<?php print $content; ?>
	</div> <!-- /content-content -->
	<?php print $feed_icons; ?>
    </div><!--/content-->

    <?php if (!empty($left)): ?>
    <div id="sidebar" class="column sidebar">
      <?php print $left; ?>
    </div> <!-- /sidebar-left -->
    <?php endif; ?>


  </div><!--/container-->
  <div id="footer-wrapper">
    <div id="footer">
    <?php print $footer_message; ?>
    <?php if (!empty($footer)): print $footer; endif; ?>
    </div> <!-- /footer -->
  </div> <!-- /footer-wrapper -->
<?php print $closure; ?>
</body>
</html>